# Drone types

## Rules

- Use English for whole code, names and everything
- Write code with PEP8 and the Zen of Python
- Use GitFlow branch model
- Create merge-requests (assign to Marcin Kornat)

## TODO

- **Device Status - LTE**
- **Device Status - Radio**

## Team

- Adam Konieczny - Maintainer
- Marcin Kornat - Maintainer
- Andrzej Michalski - Developer

